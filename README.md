# Colours in MATLAB

Some colour palettes for MATLAB plotting.
___
All palettes are sourced from other sites and are referenced with a link.
The colours are presented with their RGB vector.
MATLAB can read it and recreate the colour in a *figure*.

